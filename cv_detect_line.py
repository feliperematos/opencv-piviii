import cv2
import numpy as np
import math

cam = cv2.VideoCapture(0)

kernel = np.ones((5 ,5), np.uint8)


def direction(ang):
	if (ang == 90):
		direcao = "Em frente"
	elif (ang > -89 and ang < -10):
		direcao = "Movimente p/ Direita"
	elif (ang < 89 and ang > 10):
		direcao = "Movimente p/ Esquerda"
	else:
		direcao = "Parado"

	return direcao


def Stop(orig_frame, kernel):

	frame = cv2.GaussianBlur(orig_frame, (5, 5), 0)
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	up_red = np.array([255, 50, 50])
	low_red = np.array([51, 0, 0])
	mask = cv2.inRange(frame, low_red, up_red)

	edges = cv2.Canny(mask, 30, 200)
	lines = cv2.HoughLinesP(edges, 1, np.pi/180, 50, maxLineGap=50)
	
	cv2.imshow("Super teste", mask)
	
	if lines is not None:
		return "Pousar Drone"


while True:

    ret, orig_frame = cam.read()
    frame_perf = orig_frame
    
    frame = cv2.GaussianBlur(orig_frame, (5, 5), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    low_yellow = np.array([18, 94, 140])
    up_yellow = np.array([48, 255, 255])
    mask = cv2.inRange(hsv, low_yellow, up_yellow)
    
    edges = cv2.Canny(mask, 30, 200)
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 50, maxLineGap=50)

    """
    cv2.drawContours(frame,[box],0,(0,0,255),3)
    cv2.putText(frame,str(ang),(10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
    cv2.putText(frame,str(error),(10, 320), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    cv2.putText(frame,str(movimento),(10, 400), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    cv2.line(frame, (int(x_min),200 ), (int(x_min),250 ), (255,0,0),3)"""
 
    if lines is not None:
        for line in lines:
            #print("Contador: "+str(count)+"  Linha: "+str(line)+"\n")

            x1, y1, x2, y2 = line[0]

            if (x1 < 220 and x2 < 220):
                cv2.rectangle(frame, (220, 0), (450, 479), (0, 0, 255))
                movimento = "Fora de pos., mover p/ Direita"
                cv2.putText(frame,str(movimento),(10, 400), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            elif (x1 > 450 and x2 > 450):
                cv2.rectangle(frame, (220, 0), (450, 479), (0, 0, 255))
                movimento = "Fora de pos., mover p/ Direita"
                cv2.putText(frame,str(movimento),(10, 400), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            else:               
                tg = (y2-y1)/(x2-x1)
                ang = math.degrees(-1*math.atan(tg))

                if (ang >= 0 and ang < 10) or (ang <= 0 and ang > -10):
                	passaport = 0
                	#print('Angulo ignorado: {}°'.format(ang))
                	#cv2.line(frame, (x1, y1), (x2, y2), (0, 0, 255), 5)
                else:
                    cv2.line(frame, (x1, y1), (x2, y2), (0, 255, 0), 5)
                    movimento = direction(ang)
                    cv2.putText(frame,str(movimento),(10, 400), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                    cv2.putText(frame,str(round(ang, 2)),(10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
                    print('Angulo: {}°'.format(ang))

                    if Stop(orig_frame, kernel) == "Pousar Drone":
                        print("Pouse")


    cv2.imshow("frame", frame)
    cv2.imshow("Teste", orig_frame)
    key = cv2.waitKey(1)
    if key == 27:
        break