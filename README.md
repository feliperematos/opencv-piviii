# Opencv-PiVIII
Comandos para o Drone:

**Funcionalidades:
	
- Deteção de linhas Amarelas.
- Filtro de linhas com -10° a 10°, essas linhas são descartadas pelo com o fim de diminuir os ruidos
- É 'printado' na tela a direção em que o Drone deve se movimentar, genericamente (Direita, Esquerda e Frente)
- O OpenCV detecta a cor azul também, se detectado é enviado uma mensagem de parada do Drone. Vale salientar que a percepção da cor azul só é ativada, se houver previamente uma liinha amarela sendo detectada.
- As linhas precisam estar sempre iniciadas/terminadas no centro da camera. Caso a linha seja detectada fora da area do centro, será mostrado o comando com a direção que se deve virar para a detecção voltar ao normal.
- Também é mostrado o angulo atual.